#include <cstdio>
#include <vector>
#include <unistd.h>

#include "Headers/UDPServer.hpp"
#include "Headers/ConcurrentQueue.hpp"
#include "Headers/UDPClient.hpp"
#include "Headers/ModuleDesignations.hpp"
#include "MAVLink/common/mavlink.h"
#include "MAVLink/common/mavlink_msg_heartbeat.h"
#include "Headers/ConcurrentFileLogger.hpp"

const int DATA_LIMIT_SIZE = 3000;
const std::string KEY = "InternationalSpaceForce";
const std::string LOOPBACK_ADDRESS = "127.0.0.1";
const int HEARTBEAT_MSG_SEND_RATE_INDEX = 1;
const int ROVER_PASSTHROUGH_IP_INDEX = 2;

ConcurrentFileLogger logger("RoverComModuleLog.txt");

std::stringstream MAVLinkMsgToStringStream(mavlink_message_t msg, bool isIncomingData) {
    std::stringstream result;
    if(isIncomingData) {
        result << "Incoming ";
    } else {
        result << "Outgoing ";
    }
    result << "MAVLinkMsg: " << (uint32_t)msg.sysid << ' ' << (uint32_t)msg.compid << ' ' << msg.msgid << ' ';
    result << msg.checksum << '\n';
    if(msg.msgid == MAVLINK_MSG_ID_COMMAND_INT) {
        mavlink_command_int_t mavCmdInt;
        mavlink_msg_command_int_decode(&msg, &mavCmdInt);
        result << "CmdID: " << mavCmdInt.command << std::endl;
    }
    return result;
}

std::vector<char> encryptData(std::vector<char> rawData, std::vector<char> key) {
    std::vector<char> encryptedData({});

    for(int index = 0; index < rawData.size(); index++) {
        if (rawData[index] != '\n')
            encryptedData.push_back((char)rawData[index] ^ key[index % key.size()]);
        else
            encryptedData.push_back(rawData[index]);
    }

    return encryptedData;
}

void ApplyXorBitMaskToData(int rawDataCount, char rawData[], char maskedAppliedData[]) {
    for (int i = 0; i < rawDataCount - 1; i++) {
        if (rawData[i] != '\n') {
            int keyModuloIndex = i % KEY.size();
            maskedAppliedData[i] = (char) (rawData[i] ^ KEY[keyModuloIndex]);
        }
        else
            maskedAppliedData[i] = rawData[i];
    }
}

std::vector<char> EncryptOrDecryptData(std::vector<char> rawData) {
    std::vector<char> encryptedData({});

    for(int index = 0; index < rawData.size(); index++) {
        if (rawData[index] != '\n')
            encryptedData.push_back((char)rawData[index] ^ KEY[index % KEY.size()]);
        else
            encryptedData.push_back(rawData[index]);
    }

    return encryptedData;
}

std::vector<char> CharArrayToVector(char* inputData, int inputDataSize) {
    std::vector<char> result(inputData, inputData + inputDataSize);
    return result;
}

void VectorToCharArray(std::vector<char> inputData, char* outputData) {
    if(inputData.size() <= DATA_LIMIT_SIZE) {
        std::copy(inputData.begin(), inputData.end(), outputData);
    }
}

[[noreturn]] void ParseMessagesInQueue(ConcurrentQueue<std::vector<char>>& messagesToParse,
                                       ConcurrentQueue<std::vector<char>>& messagesToUltrasonicSensorModule,
                                       ConcurrentQueue<std::vector<char>>& messagesToIEDSensorModule,
                                       ConcurrentQueue<std::vector<char>>& messagesToPathPlanningModule,
                                       ConcurrentQueue<std::vector<char>>& messagesToMotorControlModule,
                                       ConcurrentQueue<std::vector<char>>& messagesToSoldierRetrievalModule,
                                       ConcurrentQueue<std::vector<char>>& messagesToCameraControlModule,
                                       ConcurrentQueue<std::vector<char>>& messagesToCentralExecutiveModule,
                                       ConcurrentQueue<std::vector<char>>& outGoingExternalInterfaceMsgs) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;
        std::vector<char> dataToBeSent;

        messagesToParse.pop(dataToBeSent);
        receivedByteCount = dataToBeSent.size();
        VectorToCharArray(dataToBeSent, receivedBuffer);

        mavlink_message_t msg;
        mavlink_status_t status;
        mavlink_heartbeat_t heartbeat;
        unsigned int firstCharInBuffer = -1;
        bool dataWasCorrectlyParsed = false;

        for (int bufferIndex = 0; bufferIndex < receivedByteCount; ++bufferIndex) {
            firstCharInBuffer = dataToBeSent[bufferIndex];
            //printf("%02x ", (unsigned char) firstCharInBuffer);
            if (mavlink_parse_char(MAVLINK_COMM_0, dataToBeSent[bufferIndex], &msg, &status)) {
                //mavlink_msg_heartbeat_decode(&msg, &heartbeat);
                // Log all parsed messages
                dataWasCorrectlyParsed = true;
                logger.WriteToFile(MAVLinkMsgToStringStream(msg, true));
                switch (msg.compid) {
                    case UltrasonicSensor:
                        messagesToUltrasonicSensorModule.push(dataToBeSent);
                        break;

                    case IEDSensor:
                        messagesToIEDSensorModule.push(dataToBeSent);
                        break;

                    case PathPlanning:
                        messagesToPathPlanningModule.push(dataToBeSent);
                        break;

                    case MotorControl:
                        messagesToMotorControlModule.push(dataToBeSent);
                        break;

                    case SoldierRetrieval:
                        messagesToSoldierRetrievalModule.push(dataToBeSent);
                        break;

                    case CameraControl:
                        messagesToCameraControlModule.push(dataToBeSent);
                        break;

                    case CentralExecutive:
                        messagesToCentralExecutiveModule.push(dataToBeSent);
                        break;

                    case GUI:
                    case MissionExecutive:
                        // Outgoing External Interface to CCS
                        outGoingExternalInterfaceMsgs.push(dataToBeSent);
                        break;

                    default:
                        logger.WriteToFile("Invalid MAVLink Data Received\n");
                        break;
                }
            }
        }
        if(!dataWasCorrectlyParsed) {
            logger.WriteToFile("Invalid Data Received\n");
        }
    }
}

[[noreturn]] void WaitForDataToBeReceived(UDPServer& serverConnection,
                                          ConcurrentQueue<std::vector<char>>& dataReceived) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;

        //printf("Waiting for local data: \n");
        serverConnection.GetDatagram(std::ref(receivedByteCount), receivedBuffer, DATA_LIMIT_SIZE);
        //printf("Received local data: %s\n", receivedBuffer);

        dataReceived.push(CharArrayToVector(receivedBuffer, receivedByteCount));
    }
}

[[noreturn]] void SendDataFromQueue(UDPClient& clientModuleConnection, ConcurrentQueue<std::vector<char>>& queue) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;
        std::vector<char> dataToBeSent;

        queue.pop(dataToBeSent);
        receivedByteCount = dataToBeSent.size();

        VectorToCharArray(dataToBeSent, receivedBuffer);

        //printf("Sending Data\n");
        clientModuleConnection.SentDatagramToServerSuccessfully(receivedBuffer, receivedByteCount);
    }
}

[[noreturn]] void SendDataToCCS(UDPClient& ccsConnection, ConcurrentQueue<std::vector<char>>& dataToSend) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;
        std::vector<char> dataToBeSent;

        dataToSend.pop(dataToBeSent);
        receivedByteCount = dataToBeSent.size();

        // Encrypt data to send over to Rover
        std::vector<char> encryptedData = EncryptOrDecryptData(dataToBeSent);
        VectorToCharArray(encryptedData, receivedBuffer);

        //printf("Sending Data\n");
        ccsConnection.SentDatagramToServerSuccessfully(receivedBuffer, receivedByteCount);
    }
}

[[noreturn]] void ReceiveDataFromCCS(UDPServer& ccsConnection, ConcurrentQueue<std::vector<char>>& incomingData) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;

        //printf("Waiting for CCS data: \n");
        ccsConnection.GetDatagram(std::ref(receivedByteCount), receivedBuffer, DATA_LIMIT_SIZE);
        //printf("Received CCS: %s\n", receivedBuffer);

        // Need to decrypt raw data from Rover
        std::vector<char> decryptedData = EncryptOrDecryptData(CharArrayToVector(receivedBuffer, receivedByteCount));
        incomingData.push(decryptedData);
    }
}

[[noreturn]] void SendHeartbeatStatusMessage(ConcurrentQueue<std::vector<char>>& dataQueue, int sendRateInSeconds) {
    while(true) {
        /* Send Heartbeat */
        uint8_t buf[DATA_LIMIT_SIZE];
        mavlink_message_t msg;
        uint16_t len;
        mavlink_msg_heartbeat_pack(RoverCommunication, CentralExecutive, &msg, MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC, MAV_MODE_GUIDED_ARMED, 0, MAV_STATE_ACTIVE);
        len = mavlink_msg_to_send_buffer(buf, &msg);

        std::vector<char> dataToSend = CharArrayToVector((char*)buf, len);
        dataQueue.push(dataToSend);

        // Sleep for X seconds
        sleep(sendRateInSeconds);
    }
}

int main(int argc, char *argv[]) {
    int heartbeatSendRateInSecs = 10;
    std::string passthroughIP = LOOPBACK_ADDRESS;
    int passthroughListeningPort = RoverPassthroughModule;

    if(argc > 1) {
        heartbeatSendRateInSecs = std::stoi(argv[HEARTBEAT_MSG_SEND_RATE_INDEX]);
        passthroughIP = argv[ROVER_PASSTHROUGH_IP_INDEX];
        passthroughListeningPort = EncryptedFromRoverData;
    }

    // Concurrent queues used for data transmission
    // External to CCS interfaces
    ConcurrentQueue<std::vector<char>> dataToSendToCCS;
    UDPClient sendDataToCCS(passthroughListeningPort, passthroughIP);
    UDPServer receiveDataFromCCS(EncryptedFromCCSData, LOOPBACK_ADDRESS);

    // Internal to CCS interfaces
    ConcurrentQueue<std::vector<char>> incomingDataToParse;
    ConcurrentQueue<std::vector<char>> dataToSendToUltrasonicSensorModule;
    ConcurrentQueue<std::vector<char>> dataToSendToIEDSensorModule;
    ConcurrentQueue<std::vector<char>> dataToSendToPathPlanningModule;
    ConcurrentQueue<std::vector<char>> dataToSendToMotorControlModule;
    ConcurrentQueue<std::vector<char>> dataToSendToSoldierRetrievalModule;
    ConcurrentQueue<std::vector<char>> dataToSendToCameraControlModule;
    ConcurrentQueue<std::vector<char>> dataToSendToCentralExecutiveModule;

    // Receivers and senders for data transmission
    UDPServer thisServer(RoverCommunicationModule, LOOPBACK_ADDRESS);
    UDPClient connectionToUltrasonicSensorModule(UltrasonicSensorModule, LOOPBACK_ADDRESS);
    UDPClient connectionToIEDSensorModule(IEDSensorModule, LOOPBACK_ADDRESS);
    UDPClient connectionToPathPlanningModule(PathPlanningModule, LOOPBACK_ADDRESS);
    UDPClient connectionToMotorControlModule(MotorControlModule, LOOPBACK_ADDRESS);
    UDPClient connectionToSoldierRetrievalModule(SoldierRetrievalModule, LOOPBACK_ADDRESS);
    UDPClient connectionToCameraControlModule(CameraControlModule, LOOPBACK_ADDRESS);
    UDPClient connectionToCentralExecutiveModule(CentralExecutiveModule, LOOPBACK_ADDRESS);

    // External interface connection
    std::thread sendToCCS(SendDataToCCS, std::ref(sendDataToCCS), std::ref(dataToSendToCCS));
    std::thread receiveFromCCS(ReceiveDataFromCCS, std::ref(receiveDataFromCCS), std::ref(incomingDataToParse));

    // Main server functions for incoming data
    std::thread receiveData(WaitForDataToBeReceived, std::ref(thisServer), std::ref(incomingDataToParse));
    std::thread parseDataReceived(ParseMessagesInQueue, std::ref(incomingDataToParse),
                                  std::ref(dataToSendToUltrasonicSensorModule),
                                  std::ref(dataToSendToIEDSensorModule),
                                  std::ref(dataToSendToPathPlanningModule),
                                  std::ref(dataToSendToMotorControlModule),
                                  std::ref(dataToSendToSoldierRetrievalModule),
                                  std::ref(dataToSendToCameraControlModule),
                                  std::ref(dataToSendToCentralExecutiveModule),
                                  std::ref(dataToSendToCCS));

    // Main server functions for outgoing data
    std::thread sendHeartbeatMsgToCenExecMod(SendHeartbeatStatusMessage,
            std::ref(incomingDataToParse), heartbeatSendRateInSecs);
    std::thread sendDataToUltrasonicSensorModule(SendDataFromQueue, std::ref(connectionToUltrasonicSensorModule),
                                    std::ref(dataToSendToUltrasonicSensorModule));
    std::thread sendDataToIEDSensorModule(SendDataFromQueue, std::ref(connectionToIEDSensorModule),
                                                 std::ref(dataToSendToIEDSensorModule));
    std::thread sendDataToPathPlanningModule(SendDataFromQueue, std::ref(connectionToPathPlanningModule),
                                                 std::ref(dataToSendToPathPlanningModule));
    std::thread sendDataToMotorControlModule(SendDataFromQueue, std::ref(connectionToMotorControlModule),
                                                 std::ref(dataToSendToMotorControlModule));
    std::thread sendDataToSoldierRetrievalModule(SendDataFromQueue, std::ref(connectionToSoldierRetrievalModule),
                                                 std::ref(dataToSendToSoldierRetrievalModule));
    std::thread sendDataToCameraControlModule(SendDataFromQueue, std::ref(connectionToCameraControlModule),
                                                 std::ref(dataToSendToCameraControlModule));
    std::thread sendDataToCentralExecutiveModule(SendDataFromQueue, std::ref(connectionToCentralExecutiveModule),
                                                 std::ref(dataToSendToCentralExecutiveModule));

    printf("Rover Communication Module Executing\n");
    printf("Connecting Channel Link\n");
    printf("Using Encrypted Channel Link\n");
    logger.WriteToFile("Connecting Channel Link");
    logger.WriteToFile("Using Encrypted Channel Link");

    sendToCCS.join();
    receiveFromCCS.join();
    receiveData.join();
    parseDataReceived.join();
    sendHeartbeatMsgToCenExecMod.join();
    sendDataToUltrasonicSensorModule.join();
    sendDataToIEDSensorModule.join();
    sendDataToPathPlanningModule.join();
    sendDataToMotorControlModule.join();
    sendDataToSoldierRetrievalModule.join();
    sendDataToCameraControlModule.join();
    sendDataToCentralExecutiveModule.join();

    return 0;
}
